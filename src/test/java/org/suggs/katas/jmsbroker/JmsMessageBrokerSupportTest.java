package org.suggs.katas.jmsbroker;

import exceptions.NoMessageReceivedException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.suggs.katas.jmsbroker.JmsMessageBrokerSupport.*;


public class JmsMessageBrokerSupportTest {

    public static final String TEST_QUEUE = "MY_TEST_QUEUE";
    public static final String MESSAGE_CONTENT = "Lorem blah blah";
    private static JmsMessageBrokerSupport JMS_SUPPORT;
    private static String REMOTE_BROKER_URL;

    @BeforeClass
    public static void setup() throws Exception {
        JMS_SUPPORT = createARunningEmbeddedBrokerOnAvailablePort();  //        tcp://localhost:41617
        REMOTE_BROKER_URL = JMS_SUPPORT.getBrokerUrl();
    }

    @AfterClass
    public static void teardown() throws Exception {
        JMS_SUPPORT.stopTheRunningBroker();
    }

    @Test
    public void sendsMessagesToTheRunningBroker() throws Exception {
        bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .andThen().sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT);
        long messageCount = JMS_SUPPORT.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
    }

    @Test
    public void readsMessagesPreviouslyWrittenToAQueue() throws Exception {
        String receivedMessage = bindToBrokerAtUrl(REMOTE_BROKER_URL)
                .sendATextMessageToDestinationAt(TEST_QUEUE, MESSAGE_CONTENT)
                .andThen().retrieveASingleMessageFromTheDestination(TEST_QUEUE);
        assertThat(receivedMessage).isEqualTo(MESSAGE_CONTENT);
    }

    @Test(expected = NoMessageReceivedException.class)
    public void throwsExceptionWhenNoMessagesReceivedInTimeout() throws Exception {
        bindToBrokerAtUrl(REMOTE_BROKER_URL).retrieveASingleMessageFromTheDestination(TEST_QUEUE, 1);
    }

    @Test
    public void testCreateARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        assertThat(JMS_SUPPORT).isNotNull();
    }

    @Test
    public void testIsEmptyQueueAt() throws Exception {
        assertThat(JMS_SUPPORT.isEmptyQueueAt(TEST_QUEUE)).isTrue();
    }


    @Test
    public void testActiveMq() throws Exception {
        JMS_SUPPORT.bindToActiveMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(MESSAGE_CONTENT).to(TEST_QUEUE)
                .andThen().waitForAMessageOn(TEST_QUEUE);
    }

    @Test
    public void testSendAMessageToActiveMq() throws Exception {
        JMS_SUPPORT.bindToActiveMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(MESSAGE_CONTENT).to(TEST_QUEUE);
        long messageCount = JMS_SUPPORT.getEnqueuedMessageCountAt(TEST_QUEUE);
        assertThat(messageCount).isEqualTo(1);
        JMS_SUPPORT.waitForAMessageOn(TEST_QUEUE);
    }

    @Test(expected = NotImplementedException.class)
    public void testIbmMq() throws Exception {
        JMS_SUPPORT.bindToIbmMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(MESSAGE_CONTENT).to(TEST_QUEUE)
                .andThen().waitForAMessageOn(TEST_QUEUE);
    }

    @Test(expected = NotImplementedException.class)
    public void testTibcoMq() throws Exception {
        JMS_SUPPORT.bindToTibcoMqBrokerAt(REMOTE_BROKER_URL)
                .and().sendTheMessage(MESSAGE_CONTENT).to(TEST_QUEUE)
                .andThen().waitForAMessageOn(TEST_QUEUE);
    }
}
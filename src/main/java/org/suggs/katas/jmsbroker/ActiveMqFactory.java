package org.suggs.katas.jmsbroker;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

public class ActiveMqFactory implements ConnectionFactory {

    @Override
    public Connection createConnection() throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(((UrlMaker) Utils.getContext().getBean("urlMaker")).getUrl());
        return connectionFactory.createConnection();
    }

    @Override
    public Connection createConnection(String userName, String password) throws JMSException {
        return this.createConnection();
    }
}

package org.suggs.katas.jmsbroker;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Utils {

    private static ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

    public static ClassPathXmlApplicationContext getContext(){
        return ctx;
    }
}

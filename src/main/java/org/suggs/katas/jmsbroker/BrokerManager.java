package org.suggs.katas.jmsbroker;

import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.slf4j.Logger;

import static org.slf4j.LoggerFactory.getLogger;

public class BrokerManager {

    BrokerService brokerService;

    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);

    public void createEmbeddedBroker(String aBrokerUrl) throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(aBrokerUrl);
//        brokerService.addConnector(aBrokerUrl);
    }

    public void createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {  /*TODO !!!!!!!!!!!!!!*/
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
//        JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
        createEmbeddedBroker(aBrokerUrl);
        startEmbeddedBroker();
    }

    public void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }

    public void stopTheRunningBroker() throws Exception {
        if (brokerService == null) {
            throw new IllegalStateException("Cannot stop the broker from this API: " +
                    "perhaps it was started independently from this utility");
        }
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    public DestinationStatistics getDestinationStatisticsFor(String aDestinationName) throws Exception {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, ((UrlMaker) Utils.getContext().getBean("urlMaker")).getUrl()));
    }

}

package org.suggs.katas.jmsbroker;

import org.slf4j.Logger;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import static org.slf4j.LoggerFactory.getLogger;

abstract class JmsCallback {


    private ConnectionFactory connectionFactory;

    private static final Logger LOG = getLogger(JmsCallback.class);

    protected JmsCallback(ConnectionFactory cf) {
        this.connectionFactory = cf;
    }

    public Connection getConnection() {
        Connection connection;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", ((UrlMaker) Utils.getContext().getBean("urlMaker")).getUrl());
            throw new IllegalStateException(jmse);
        }
        return connection;
    }
}
package org.suggs.katas.jmsbroker;

import exceptions.NoMessageReceivedException;
import org.slf4j.Logger;

import javax.jms.*;
import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;

public class MessageReceiver extends JmsCallback {

    private static final Logger LOG = getLogger(MessageReceiver.class);

    public MessageReceiver(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    public String receiveMessageFrom(String destinationName, int timeout) {
        Session session = null;
        try {
            session = getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(destinationName);
            MessageConsumer consumer = session.createConsumer(queue);
            TextMessage message = (TextMessage) consumer.receive(timeout);
            if (message == null) {
                throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", timeout));
            }
            consumer.close();
            return message.getText();
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", getConnection());
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
}

package org.suggs.katas.jmsbroker;

import org.slf4j.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.slf4j.LoggerFactory.getLogger;

public class JmsMessageBrokerSupport {
    private static final Logger LOG = getLogger(JmsMessageBrokerSupport.class);
    private static final int ONE_SECOND = 1000;
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;

    private static String brokerUrl;
    private static BrokerManager brokerManager = ((BrokerManager) Utils.getContext().getBean("brokerManager"));
    private String brokerBeanNamePrefix = "activeMq";
    private String messageBody;

    private JmsMessageBrokerSupport() {
    }

    @Deprecated
    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(((UrlMaker) Utils.getContext().getBean("urlMaker")).getUrl());
    }

    @Deprecated
    public static JmsMessageBrokerSupport createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBrokerSupport broker = bindToBrokerAtUrl(aBrokerUrl);
        brokerManager.createARunningEmbeddedBrokerAt(aBrokerUrl);
        brokerUrl = aBrokerUrl;
        return broker;
    }


    @Deprecated
    public static JmsMessageBrokerSupport bindToBrokerAtUrl(String aBrokerUrl) throws Exception {
        JmsMessageBrokerSupport jmsMessageBrokerSupport = new JmsMessageBrokerSupport();
        jmsMessageBrokerSupport.setUrl(aBrokerUrl);
        return jmsMessageBrokerSupport;
    }

    @Deprecated
    public void stopTheRunningBroker() throws Exception {
        brokerManager.stopTheRunningBroker();
    }

    public JmsMessageBrokerSupport and(){
        return this;
    }

    public final JmsMessageBrokerSupport andThen() {
        return this;
    }

    public final String getBrokerUrl() {
        return brokerUrl;
    }

    @Deprecated
    public JmsMessageBrokerSupport sendATextMessageToDestinationAt(String aDestinationName, final String aMessageToSend) {
        ((MessageSender) Utils.getContext().getBean(brokerBeanNamePrefix+"Sender")).sendMessageTo(aDestinationName, aMessageToSend);
        return this;
    }

    @Deprecated
    public String retrieveASingleMessageFromTheDestination(String aDestinationName) {
        return retrieveASingleMessageFromTheDestination(aDestinationName, DEFAULT_RECEIVE_TIMEOUT);
    }

    @Deprecated
    public String retrieveASingleMessageFromTheDestination(String aDestinationName, final int aTimeout) {
        return ((MessageReceiver) Utils.getContext().getBean(brokerBeanNamePrefix + "Receiver")).receiveMessageFrom(aDestinationName, aTimeout);
    }

    public long getEnqueuedMessageCountAt(String aDestinationName) throws Exception {
        return brokerManager.getDestinationStatisticsFor(aDestinationName).getMessages().getCount();
    }

    public boolean isEmptyQueueAt(String aDestinationName) throws Exception {
        return getEnqueuedMessageCountAt(aDestinationName) == 0;
    }

    public JmsMessageBrokerSupport bindToActiveMqBrokerAt(String url) throws Exception {
        setUrl(url);
        setBrokerBeanNamePrefix("activeMq");
        return this;
    }

    private void setUrl(String url) {
        ((UrlMaker) Utils.getContext().getBean("urlMaker")).setUrl(url);
    }

    public JmsMessageBrokerSupport sendTheMessage(String messageText) {
        setMessageBody(messageText);
        return this;
    }

    public JmsMessageBrokerSupport to(String destinationName){
        ((MessageSender) Utils.getContext().getBean(brokerBeanNamePrefix+"Sender")).sendMessageTo(destinationName, messageBody);
        return this;
    }

    public String waitForAMessageOn(String destinationName){
        return ((MessageReceiver) Utils.getContext().getBean(brokerBeanNamePrefix + "Receiver")).receiveMessageFrom(destinationName, DEFAULT_RECEIVE_TIMEOUT);
    }


    public JmsMessageBrokerSupport bindToIbmMqBrokerAt(String url) throws Exception {
        setUrl(url);
        throw new NotImplementedException();
//        setBrokerBeanNamePrefix("ibmMq");
//        return this;
    }

    public JmsMessageBrokerSupport bindToTibcoMqBrokerAt(String url) throws Exception {
        setUrl(url);
        throw new NotImplementedException();
//        setBrokerBeanNamePrefix("tibcoMq");
//        return this;
    }

    public void setBrokerBeanNamePrefix(String brokerBeanNamePrefix) {
        this.brokerBeanNamePrefix = brokerBeanNamePrefix;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}

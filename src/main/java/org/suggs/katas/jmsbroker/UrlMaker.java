package org.suggs.katas.jmsbroker;

public class UrlMaker {

    public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";

    String url;

    public UrlMaker() {
        url = DEFAULT_BROKER_URL_PREFIX + SocketFinder.findNextAvailablePortBetween(41616, 50000);
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

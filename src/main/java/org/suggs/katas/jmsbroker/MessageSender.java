package org.suggs.katas.jmsbroker;

import org.slf4j.Logger;

import javax.jms.*;
import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;

public class MessageSender extends JmsCallback {

    private static final Logger LOG = getLogger(MessageSender.class);

    public MessageSender(ConnectionFactory connectionFactory) {
        super(connectionFactory);
    }

    public void sendMessageTo(String destinationName, String text) {
        Session session = null;
        try {
            session = getConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue(destinationName);
            MessageProducer producer = session.createProducer(queue);
            producer.send(session.createTextMessage(text));
            producer.close();
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", getConnection());
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
}
